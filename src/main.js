import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueSwal from 'vue-swal'
import axios from 'axios'
import VueAxios from 'vue-axios'

axios.defaults.baseURL = 'http://167.99.7.234';
Vue.use(VueAxios, axios);

Vue.use(VueSwal);
Vue.use(Vuetify);
Vue.use(require('vue-moment'));

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
