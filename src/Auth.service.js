import auth0 from 'auth0-js'

export default class Authenticator {
    constructor() {
        this.auth0 = new auth0.WebAuth({
            domain: 'dev-u311zl0s.auth0.com',
            clientID: 'kPvy6DzgZnp1hg0tz7P8obd2fCCD1JDg',
            redirectUri: 'http://159.89.186.240/auth',
            audience: '',
            responseType: 'token id_token',
            scope: 'openid email profile'
        });
    }

    requireAuth(to, from, next) {
        if (! (new Authenticator).isAuthenticated()) {
            next({
                path: '/',
                query: { redirect: to.fullPath }
            });
        } else {
            next();
        }
    }

    handleAuthentication() {
        return new Promise((resolve, reject) => {
            this.auth0.parseHash((err, authResult) => {
                if (err) return reject(err);

                resolve(authResult);
            });
        });
    }

    isAuthenticated() {
        const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
        return new Date().getTime() < expiresAt;
    }

    login() {
        this.auth0.authorize();
    }
};