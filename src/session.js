import Authenticator from './Auth.service'

import router from './router';

const auth = new Authenticator();

const state = {
    authenticated: !!localStorage.getItem('access_token'),
    accessToken: localStorage.getItem('access_token'),
    idToken: localStorage.getItem('id_token'),
    expiresAt: localStorage.getItem('expires_at'),
    nickname: localStorage.getItem('nickname')
};

const getters = {
    idToken (state) {
        return state.nickname;
    },
    authenticated (state) {
        return state.authenticated;
    }
};

const mutations = {
    authenticated (state, authData) {
        state.authenticated = true;
        state.accessToken = authData.accessToken;
        state.idToken = authData.idToken;
        state.expiresAt = authData.expiresIn * 1000 + new Date().getTime();
        state.nickname = authData.idTokenPayload.nickname;
        // console.log(authData);
        
        localStorage.setItem('access_token', state.accessToken);
        localStorage.setItem('id_token', state.idToken);
        localStorage.setItem('expires_at', state.expiresAt);
        localStorage.setItem('nickname', state.nickname);
    },

    logout (state) {
        state.authenticated = false;
        state.accessToken = null;
        state.idToken = false;
        state.nickname = null;

        localStorage.removeItem('access_token');
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        localStorage.removeItem('nickname');

        router.push('/');
    }
};

const actions = {
    login() {
        auth.login();
    },
    logout({ commit }) {
        commit('logout');
    },
    handleAuthentication({ commit }) {
        auth.handleAuthentication().then((authResult) => {
            commit('authenticated', authResult);
        }).catch((err) => {
            console.log(err);
        });
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}