import Vue from 'vue'
import Router from 'vue-router'

import Home from './views/Home.vue'
import Kiosks from './views/Kiosks.vue'
import Users from './views/Users.vue'
import Listings from './views/Listings.vue'
import Stats from './views/Stats.vue'
import Settings from './views/Settings.vue'
import Auth from './views/Auth.vue'
import Changelog from './views/Changelog.vue'

import AuthService from './Auth.service'

const auth = new AuthService();

Vue.use(Router);

var router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/changes',
      name: 'changes',
      component: Changelog
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth
    },
    {
      path: '/',
      name: 'dashboard',
      component: Home
    },
    {
      path: '/kiosks',
      name: 'kiosks',
      beforeEnter: auth.requireAuth,
      component: Kiosks
    },
    {
      path: '/users',
      name: 'users',
      beforeEnter: auth.requireAuth,
      component: Users
    },
    {
      path: '/analytics',
      name: 'stats',
      // beforeEnter: auth.requireAuth,
      component: Stats
    },
    {
      path: '/settings',
      name: 'settings',
      beforeEnter: auth.requireAuth,
      component: Settings
    },
    {
      path: '/listings',
      name: 'listings',
      beforeEnter: auth.requireAuth,
      component: Listings
    }
  ]
});

export default router;